﻿using UnityEngine;

namespace XChangesTracking
{
    [System.Serializable]
    public abstract class CT_Base : XUtils.NullableClass {
        public virtual void register() { }

        public abstract bool hasValueChanges();
        public abstract bool applyChanges();
        public abstract bool revertChanges();
    }

    [System.Serializable]
    public abstract class CT_Simple<T_Type> : CT_Base {
        public CT_Simple() {
            setAsNotNull();
        }

        public override void register() {
            _value = _nextValue;
        }

        public T_Type getValue() { return _value; }
        public T_Type getNextValue() { return _nextValue; }

        public void setValue(T_Type inValue) {
            setNextValue(inValue);
            //return applyChanges();
        }

        public void setNextValue(T_Type inValue) {
            _nextValue = inValue;
        }

        public sealed override bool hasValueChanges() {
            return areValueDifferent(_value, _nextValue);
        }

        public sealed override bool applyChanges() {
            bool theHasValueChanges = hasValueChanges();
            _value = _nextValue;
            return theHasValueChanges;
        }

        public sealed override bool revertChanges() {
            bool theHadValueChanges = hasValueChanges();
            _nextValue = _value;
            return theHadValueChanges;
        }

        virtual protected bool areValueDifferent(T_Type inValue, T_Type inNextValue) {
            return !XUtils.areEquals(inValue, inNextValue);
        }

        [SerializeField, HideInInspector] public T_Type _value;
        [SerializeField] public T_Type _nextValue;
    }

    [System.Serializable]
    public class CT_Ref<T_RefType> : CT_Simple<T_RefType> where T_RefType : class { }
}

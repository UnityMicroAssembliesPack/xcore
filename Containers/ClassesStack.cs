#define STACK_OBJECTS_DEBUG

using static XUtils;

// ====================================================

//NB: Classes here are NOT thread safe!

public static class StackObjectsTypes
{
#   if STACK_OBJECTS_DEBUG
    internal static bool sIsDiagnosticPrintEnabled = false;
#   endif

    // ----------------------------------------------------------

    //TODO: Use for manual managed objects
    public interface IManualLifecycleObject : System.IDisposable {
    }

    // ----------------------------------------------------------

    internal interface IStackElementLifecycleManager<T_ElementType>
        where T_ElementType : class
    {
        T_ElementType allocateElement();
        void freeElement(T_ElementType inElement);
        void initElement(T_ElementType inElement);
    }

    internal struct DefaultStackElementLifecycleManager<T_ElementType> : IStackElementLifecycleManager<T_ElementType>
        where T_ElementType : class, new()
    {
        public T_ElementType allocateElement() { return new T_ElementType(); }
        public void freeElement(T_ElementType inElement) { }
        public void initElement(T_ElementType inElement) { }
    }

    internal struct DisposableStackElementLifecycleManager<T_ElementType> : IStackElementLifecycleManager<T_ElementType>
        where T_ElementType : class, System.IDisposable, new()
    {
        public T_ElementType allocateElement() { return new T_ElementType(); }
        public void freeElement(T_ElementType inElement) { inElement.Dispose(); }
        public void initElement(T_ElementType inElement) { }
    }

    //NB: Use this Manager with wisdom. It has global allocation size setup,
    // that used to be setup before possible allocatable action
    internal struct SizedArrayStackElementLifecycleManager<T_ArrayElementType> : IStackElementLifecycleManager<T_ArrayElementType[]>
    {
        public T_ArrayElementType[] allocateElement() {
            check(0 != sCurrentAllocationsSize);
            return new T_ArrayElementType[sCurrentAllocationsSize];
        }
        public void freeElement(T_ArrayElementType[] inElement) { }
        public void initElement(T_ArrayElementType[] inElement) { }

        public static int sCurrentAllocationsSize = 0;
    }

    // ----------------------------------------------------------

    internal class Stack<T_ElementType, T_ElementLifecycleManager> : System.IDisposable
        where T_ElementType : class
        where T_ElementLifecycleManager : struct, IStackElementLifecycleManager<T_ElementType>
    {
        public Stack() {
            performInitialExpanding();
        }

        public T_ElementType pushAndReturn() {
            expandIfNeeds();

            _usedElementsNum++;
            T_ElementType theElementToReturn = _elements[_usedElementsNum - 1];
            sElementLifecycleManager.initElement(theElementToReturn);

#           if STACK_OBJECTS_DEBUG //{
            if (sIsDiagnosticPrintEnabled)
                UnityEngine.Debug.Log(getDebugStringForAction("PUSH"));
#           endif //} STACK_OBJECTS_DEBUG

            return theElementToReturn;
        }

        public void Dispose() { popTop(); }
        private void popTop() {
            XUtils.check(0 != _usedElementsNum);

            T_ElementType theElementToFree = _elements[_usedElementsNum - 1];
            sElementLifecycleManager.freeElement(theElementToFree);
            --_usedElementsNum;

#           if STACK_OBJECTS_DEBUG //{
            if (sIsDiagnosticPrintEnabled)
                UnityEngine.Debug.Log(getDebugStringForAction("POP"));
#           endif //} STACK_OBJECTS_DEBUG
        }

#       if STACK_OBJECTS_DEBUG //{
        private string getDebugStringForAction(string inActionName) {
            return "[STACK DEBUG" +
                " | Action: " + inActionName +
                " | Stack name: " + getStackDescription() +
                " | Stack size (used/reserv): " + _usedElementsNum + " / " + _elements.Length +
                " ]";
        }

        private string getStackDescription() {
            return typeof(T_ElementType).FullName;
        }
#       endif //} STACK_OBJECTS_DEBUG

        private void performInitialExpanding() {
            int theInitialElementsNum = kResizePerRealloc;

            _elements = new T_ElementType[theInitialElementsNum];
            for (int theIndex = 0; theIndex < theInitialElementsNum; ++theIndex)
                _elements[theIndex] = sElementLifecycleManager.allocateElement();
        }

        private void expandIfNeeds() {
            if (_usedElementsNum == _elements.Length) {
                int theNewElementNum = _usedElementsNum + kResizePerRealloc;

                T_ElementType[] theOldElements = _elements;
                _elements = new T_ElementType[theNewElementNum];
        
                int theIndex = 0;

                for (; theIndex < _usedElementsNum; ++theIndex)
                    _elements[theIndex] = theOldElements[theIndex];

                for (; theIndex < theNewElementNum; ++theIndex) {
                    _elements[theIndex] = sElementLifecycleManager.allocateElement();
                }
        
                _usedElementsNum = theNewElementNum;
            }
        }

        private T_ElementType[] _elements = null;
        private int _usedElementsNum = 0;

        private static int kResizePerRealloc = 2;

        private static T_ElementLifecycleManager sElementLifecycleManager = new T_ElementLifecycleManager();
    }
}

// ====================================================

public static class StackObjects
{
    public static System.IDisposable getTop<T_Type>(out T_Type outElement)
        where T_Type : class, new()
    {
        XUtils.check(!typeof(T_Type).IsAssignableFrom(typeof(System.IDisposable)),
            "Disposable object accessed from nondisposable stack. Use getDisposableTop(...) instead"
        );

        outElement = StackTypedHolder<T_Type>.stack.pushAndReturn();
        return StackTypedHolder<T_Type>.stack;
    }

    public static System.IDisposable getDisposableTop<T_Type>(out T_Type outElement)
        where T_Type : class, System.IDisposable, new()
    {
        outElement = DisposableStackTypedHolder<T_Type>.stack.pushAndReturn();
        return DisposableStackTypedHolder<T_Type>.stack;
    }

    // - - - - - - - - - - - - - - -

    private static class StackTypedHolder<T_Type>
        where T_Type : class, new()
    {
        public static StackObjectsTypes.Stack<T_Type, StackObjectsTypes.DefaultStackElementLifecycleManager<T_Type>> stack =
            new StackObjectsTypes.Stack<T_Type, StackObjectsTypes.DefaultStackElementLifecycleManager<T_Type>>();
    }

    private static class DisposableStackTypedHolder<T_Type>
        where T_Type : class, System.IDisposable, new()
    {
        public static StackObjectsTypes.Stack<T_Type, StackObjectsTypes.DisposableStackElementLifecycleManager<T_Type>> stack =
            new StackObjectsTypes.Stack<T_Type, StackObjectsTypes.DisposableStackElementLifecycleManager<T_Type>>();
    }
}

// ====================================================

public static class ArraysStack
{
    public static System.IDisposable getTop<T_Type>(out T_Type[] outArray, int inSize) {
        return ArrayStacksManager<T_Type>.get(out outArray, inSize);
    }

    // - - - - - - - - - - - - - - - Array stack holders

    private static int kSizeRange = 10;

    private static class ArrayStacksManager<T_Type>
    {
        public static System.IDisposable get(out T_Type[] outArray, int inArraySize)
        {
            int theStackBySizeIndex = computeNeededStackBySizeIndexForArraySize(inArraySize);
            allocateStacksToSupportArraysSizeUpToSize(theStackBySizeIndex);

            StackObjectsTypes.Stack<T_Type[], StackObjectsTypes.SizedArrayStackElementLifecycleManager<T_Type>> theStack =
                stacks[theStackBySizeIndex];

            StackObjectsTypes.SizedArrayStackElementLifecycleManager<T_Type>.sCurrentAllocationsSize =
                computeArraySizeByStackBySizeIndex(inArraySize);
            outArray = theStack.pushAndReturn();

            return theStack;
        }

        private static int computeNeededStackBySizeIndexForArraySize(int inArraySize) {
            return (inArraySize / kSizeRange);
        }

        private static int computeArraySizeByStackBySizeIndex(int inStackBySizeIndex) {
            return (inStackBySizeIndex + 1) * kSizeRange;
        }

        private static void allocateStacksToSupportArraysSizeUpToSize(int inNeededStackBySizeIndex) {
            Optional<int> theCurrentLastIndex = stacks.getLastIndex();
            for (int theSizeRangeExpandingIndex = theCurrentLastIndex.isSet() ? theCurrentLastIndex.value + 1 : 0;
                theSizeRangeExpandingIndex <= inNeededStackBySizeIndex; ++theSizeRangeExpandingIndex)
            {
                StackObjectsTypes.SizedArrayStackElementLifecycleManager<T_Type>.sCurrentAllocationsSize =
                    computeArraySizeByStackBySizeIndex(theSizeRangeExpandingIndex);
                stacks.add(
                    new StackObjectsTypes.Stack<T_Type[], StackObjectsTypes.SizedArrayStackElementLifecycleManager<T_Type>>()
                );
            }
        }

        static private FastArray<StackObjectsTypes.Stack<T_Type[], StackObjectsTypes.SizedArrayStackElementLifecycleManager<T_Type>>> stacks =
            new FastArray<StackObjectsTypes.Stack<T_Type[], StackObjectsTypes.SizedArrayStackElementLifecycleManager<T_Type>>>();
    }
}

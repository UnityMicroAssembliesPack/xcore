﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Make versions tracking code more organized
//TODO: Invalidate clearing values (aka defaultValue,
// null for references) to prevent garbage objects
//TODO: Use uint for sizes
[Serializable]
public class FastArray<T_ElementType> : IEnumerable<T_ElementType>, IDisposable
{
    public delegate void ElementProcessingDelegate(T_ElementType inValue);
    public delegate bool ElementPredicate(T_ElementType inValue);
    public delegate T_ElementType ElementAllocDelegate();

    public delegate T_ElementType ElementConstructionDelegate();

    //Methods
    //-Construction
    public FastArray() {
        reallocate(kCapacityIncrement);
    }

    public FastArray(int inInitialCapacity = kCapacityIncrement) {
        reallocate(inInitialCapacity);
    }

    public FastArray(int inSizeValue = kCapacityIncrement, bool inSetupSize = false) {
        if (inSetupSize) {
            setSize(inSizeValue);
        } else {
            reallocate(inSizeValue);
        }
    }

    public FastArray(T_ElementType[] inArray) {
        _elements = inArray;
        _size = inArray.Length;
    }

    public void Dispose() {
        clear(false);
    }

    //-Accessors
    public int getSize() {
        return _size;
    }

    public Optional<int> getLastIndex() {
        return !isEmpty() ? new Optional<int>(getSize() - 1) : new Optional<int>();
    }
    
    public int getLastIndexChecked() {
        Optional<int> theLastIndex = getLastIndex();
        XUtils.check(theLastIndex.isSet());
        return theLastIndex.value;
    }

    public bool isEmpty() {
        return 0 == getSize();
    }

    public int getCapacity() {
        return (null != _elements) ? _elements.Length : 0;
    }

    //-Elements accessors
    public T_ElementType this[int inIndex] {
        get {
            validateIndex(inIndex);
            return _elements[inIndex];
        }
        set {
            validateIndex(inIndex);
            _elements[inIndex] = value;
            _validation_changesTracker.increaseChanges();
        }
    }

    public ref T_ElementType getRef(int inIndex) {
        return ref _elements[inIndex];
    }

    public Optional<int> indexOf(T_ElementType inElement) {
        int theIndex = 0;
        foreach (T_ElementType theElement in this) {
            if (XUtils.areEquals(theElement, inElement)) break;
            ++theIndex;
        }
        return new Optional<int>(theIndex, isValidIndex(theIndex));
    }

    public bool contains(T_ElementType inElement) {
        return indexOf(inElement).isSet();
    }

    public bool contains(ElementPredicate inPredicate) {
        return findElement(inPredicate).isSet();
    }

    public Optional<T_ElementType> getFirstElement() {
        return isEmpty() ? new Optional<T_ElementType>() : new Optional<T_ElementType>(_elements[0]);
    }

    public Optional<T_ElementType> getLastElement() {
        return isEmpty() ? new Optional<T_ElementType>() : new Optional<T_ElementType>(_elements[getLastIndexChecked()]);
    }

    public T_ElementType getLastElementChecked() {
        return _elements[getLastIndexChecked()];
    }

    public T_ElementType getElementOffsetedFromLastChecked(int inOffset) {
        int theIndex = getLastIndexChecked() - inOffset;
        return _elements[theIndex];
    }

    //-Adding
    public void add(T_ElementType inElement) {
        int theSize = getSize();
        int theCapacity = getCapacity();
        if (theSize == theCapacity) {
            reallocate(theCapacity + kCapacityIncrement);
        }

        _elements[_size++] = inElement;

        _validation_changesTracker.increaseChanges();
    }

    public void addUnique(T_ElementType inElement) {
        if (!contains(inElement))
            add(inElement);
    }

    public T_ElementType addWithReturn(T_ElementType inElement) {
        add(inElement);
        return getLastElement().value;
    }

    public int addWithReturnIndex(T_ElementType inElement) {
        add(inElement);
        return getLastIndexChecked();
    }

    public void addCount(int inElementsCount) {
        addCount(inElementsCount, ()=>{
            return default(T_ElementType);
        });
    }

    public void addCount(int inElementsCount, Func<T_ElementType> inInitializationAction) {
        XUtils.check(inInitializationAction);

        int theSize = getSize();
        int theCapacity = getCapacity();

        int theCapacityIncrements =
            (theSize + inElementsCount - theCapacity) %
            kCapacityIncrement;
        if (theCapacityIncrements > 0) {
            reallocate(theCapacity +
                theCapacityIncrements * kCapacityIncrement
            );
        }

        for (int theIndex = 0; theIndex < inElementsCount; ++theIndex) {
            _elements[_size++] = inInitializationAction.Invoke();
        }

        _validation_changesTracker.increaseChanges();
    }


    public void placeMovingElement(ElementMover<T_ElementType> inMover) {
        T_ElementType theElement = inMover.extract();
        add(theElement);
    }

    public void addAll(FastArray<T_ElementType> inOtherArray)
    {
        XUtils.check(inOtherArray);
        foreach (T_ElementType theElementToAdd in inOtherArray)
            add(theElementToAdd);
    }

    public void addAll(List<T_ElementType> inOtherArray) {
        foreach (T_ElementType theElementToAdd in inOtherArray) {
            add(theElementToAdd);
        }
    }

    public void add(T_ElementType[] inRawArray) {
        add(inRawArray, inRawArray.Length);
    }

    public void add(T_ElementType[] inRawArray, int inElementsNum) {
        XUtils.check(XUtils.isValid(inRawArray));
        XUtils.check(inElementsNum <= inRawArray.Length);

        for (int theIndex = 0; theIndex < inElementsNum; ++theIndex) {
            add(inRawArray[theIndex]);
        }
    }

    public void placeMovingRange(RangeMover<T_ElementType> inMover) {
        foreach(T_ElementType theElementToMove in inMover.extractAll()) {
            add(theElementToMove);
        }
    }

    public void assignFrom(T_ElementType[] inOtherArray, bool inFreeMemory = false) {
        clear(inFreeMemory);
        if (!XUtils.isValid(inOtherArray)) return;

        foreach (T_ElementType theElement in inOtherArray)
            add(theElement);

        _validation_changesTracker.increaseChanges();
    }

    public void assignFrom(FastArray<T_ElementType> inOtherArray, bool inFreeMemory = false) {
        if (XUtils.isValid(inOtherArray)) {
            clear(inFreeMemory);

            foreach (T_ElementType theElement in inOtherArray)
                add(theElement);

            _validation_changesTracker.increaseChanges();
        }
    }

    public void assignFrom(List<T_ElementType> inOtherArray, bool inFreeMemory = false) {
        clear(inFreeMemory);
        foreach (T_ElementType theElement in inOtherArray) {
            add(theElement);
        }
        _validation_changesTracker.increaseChanges();
    }

    //-Removing
    public void removeElementAt(int inIndex, bool inUseSwapRemove = false) {
        validateIndex(inIndex);

        int theLastIndex = getLastIndexChecked();
        if (inUseSwapRemove) {
            XUtils.swap(ref _elements[inIndex], ref _elements[theLastIndex]);
        } else {
            for (int theIndex = inIndex; theIndex < theLastIndex; ++theIndex) {
                _elements[theIndex] = _elements[theIndex + 1];
            }
        }

        --_size;
        _validation_changesTracker.increaseChanges();
    }

    public T_ElementType removeElementAtWithReturn(int inIndex, bool inUseSwapRemove = false) {
        validateIndex(inIndex);

        T_ElementType theResult = _elements[inIndex];
        removeElementAt(inIndex, inUseSwapRemove);
        return theResult;
    }

    public void removeLastElement() {
        validateIndex(getLastIndexChecked());

        --_size;
        _validation_changesTracker.increaseChanges();
    }

    public Optional<T_ElementType> removeLastElementWithReturn() {
        if (!isEmpty()) {
            T_ElementType theElement = _elements[--_size];
            _validation_changesTracker.increaseChanges();
            return new Optional<T_ElementType>(theElement);
        } else {
            return new Optional<T_ElementType>();
        }
    }

    public void removeLastElements(int inNumToRemove) {
        XUtils.check(inNumToRemove > 0);
        XUtils.check(getSize() >= inNumToRemove);

        _size -= inNumToRemove;
        _validation_changesTracker.increaseChanges();
    }

    public bool removeElement(T_ElementType inElement, bool inUseSwapRemove = false) {
        Optional<int> theIndex = indexOf(inElement);
        if (!theIndex.isSet()) return false;

        removeElementAt(theIndex.value, inUseSwapRemove);
        return true;
    }

    public bool removeElement(ElementPredicate inPredicate, bool inUseSwapRemove = false) {
        Optional<int> theIndex = findIndex(inPredicate);
        if (!theIndex.isSet()) return false;

        removeElementAt(theIndex.value, inUseSwapRemove);
        return true;
    }

    public Optional<T_ElementType> removeElementWithReturn(
		ElementPredicate inPredicate, bool inUseSwapRemove = false)
	{
        Optional<int> theIndex = findIndex(inPredicate);
        if (!theIndex.isSet()) return new Optional<T_ElementType>();

        var theResult = new Optional<T_ElementType>(_elements[theIndex.value]);
        removeElementAt(theIndex.value, inUseSwapRemove);
        return theResult;
    }

    public ElementMover<T_ElementType> startMovingOfElementAt(
        int inElementIndex, bool inUseSwapRemove = false)
    {
        validateIndex(inElementIndex);
        return new ElementMover<T_ElementType>(
            this, inElementIndex, inUseSwapRemove
        );
    }

    public ElementMover<T_ElementType> startMovingOfElement(
        T_ElementType inElement, bool inUseSwapRemove = false)
    {
        return startMovingOfElementAt(
            indexOf(inElement).value, inUseSwapRemove
        );
    }

    public void removeElementsInRange(int inBeginIndex, int inEndIndex,
        ElementProcessingDelegate inProcessingDelegate)
    {
        validateIndex(inBeginIndex);
        validateIndex(inEndIndex);
        XUtils.check(inBeginIndex <= inEndIndex);

        int theLastIndex = getLastIndexChecked();
        int theElementsNumToRemove = inEndIndex - inBeginIndex;
        int theRemovingElementsOffset = theElementsNumToRemove + 1;

        if (null != inProcessingDelegate) {
            for (int theIndex = inBeginIndex; theIndex < inEndIndex; ++theIndex) {
                inProcessingDelegate.Invoke(_elements[theIndex]);
            }
        }

        if (inEndIndex != theLastIndex) {
            int theLastMovingElementIndex = theLastIndex - theRemovingElementsOffset;
            for (int theIndex = inBeginIndex; theIndex <= theLastMovingElementIndex; ++theIndex) {
                _elements[theIndex] = _elements[theIndex + theRemovingElementsOffset];
            }
        }

        _size -= theElementsNumToRemove;

        _validation_changesTracker.increaseChanges();
    }

    public void removeElementsUpToEnd(int inBeginIndex) {
        removeElementsInRange(inBeginIndex, getSize() - 1, null);
    }

    public void removeElementsUpToEnd(int inBeginIndex,
        ElementProcessingDelegate inProcessingDelegate)
    {
        removeElementsInRange(inBeginIndex, getSize() - 1, inProcessingDelegate);
    }

    public void setCapacity(int inCapacity, bool inFreeUnusedMemory = true) {
        int theCapacity = getCapacity();
        if (theCapacity == inCapacity) return;

        if (getCapacity() < inCapacity || inFreeUnusedMemory) {
            reallocate(inCapacity);
        }
    }

    //TODO: Add more memory control for methods + reduce copy-paste
    //{
    public void setSize(int inNewSize) {
        XUtils.check(inNewSize >= 0); //TODO: Pass uint as size

        int theCurrentSize = getSize();
        if (theCurrentSize < inNewSize) {
            int theCurrentCapacityBlocksNum = getCapacity() / kCapacityIncrement;
            int theNewCapacityBlocksNum = Mathf.CeilToInt((float)inNewSize / kCapacityIncrement);

            if (theNewCapacityBlocksNum > theCurrentCapacityBlocksNum) {
                reallocate(theNewCapacityBlocksNum * kCapacityIncrement);
            }
        } else {
            return;
        }

        _size = inNewSize;
        _validation_changesTracker.increaseChanges();
    }

    public void setSize(int inNewSize,
        ElementConstructionDelegate inAddingDelegate,
        ElementProcessingDelegate inRemovingDelegate = null)
    {
        XUtils.check(inNewSize >= 0); //TODO: Pass uint as size

        int theCurrentSize = getSize();
        if (theCurrentSize < inNewSize) {
            int theCurrentCapacityBlocksNum = getCapacity() / kCapacityIncrement;
            int theNewCapacityBlocksNum = Mathf.CeilToInt((float)inNewSize / kCapacityIncrement);

            if (theNewCapacityBlocksNum > theCurrentCapacityBlocksNum) {
                reallocate(theNewCapacityBlocksNum * kCapacityIncrement);
            }
            if (XUtils.isValid(inAddingDelegate)) {
                for (int theIndex = theCurrentSize; theIndex < inNewSize; ++theIndex) {
                    _elements[theIndex] = inAddingDelegate.Invoke();
                }
            }
        } else if (theCurrentSize > inNewSize) {
            if (XUtils.isValid(inRemovingDelegate)) {
                for (int theIndex = theCurrentSize - 1; theIndex >= inNewSize; --theIndex) {
                    inRemovingDelegate(_elements[theIndex]);
                }
            }
        } else {
            return;
        }

        _size = inNewSize;
        _validation_changesTracker.increaseChanges();
    }
    //}

    public void setTrackingChanges(T_ElementType[] inElements,
        ElementProcessingDelegate inAddingDelegate = null,
        ElementProcessingDelegate inRemovingDelegate = null)
    {
        foreach (T_ElementType theElement in inElements) {
            if (!contains(theElement)) {
                inAddingDelegate?.Invoke(theElement);
            }
        }

        for (int theIndex = 0; theIndex < _size; ++theIndex) {
            if (!XUtils.arrayContains(inElements, _elements[theIndex])) {
                inRemovingDelegate(_elements[theIndex]);
            }
        }

        set(inElements, false);
    }

    public void set(T_ElementType[] inElements, bool inFreeMemory = false) {
        clear(inFreeMemory);
        int theSize = inElements.Length;
        for (int theIndex = 0; theIndex < theSize; ++theIndex) {
            add(inElements[theIndex]);
        }

        _validation_changesTracker.increaseChanges();
    }

    public void set(T_ElementType[] inElements, int inCount, bool inFreeMemory = false) {
        XUtils.check(inElements.Length >= inCount);

        clear(inFreeMemory);
        int theSize = inElements.Length;
        for (int theIndex = 0; theIndex < inCount; ++theIndex) {
            add(inElements[theIndex]);
        }

        _validation_changesTracker.increaseChanges();
    }

    public RangeMover<T_ElementType> startMovingAll(
        bool inFreeMemoryAfterMove = false)
    {
        return new RangeMover<T_ElementType>(
            this, 0, getSize(), inFreeMemoryAfterMove
        );
    }

    public void clear(bool inFreeMemory = true) {
        _size = 0;

        if (inFreeMemory) {
            _elements = null;
        }
    }

    //-Iteration
    public IEnumerator<T_ElementType> GetEnumerator() {
        //NB: We cannot use foreach by elements here,
        // we iterate over filled elements only!
        int theSize = getSize();
        for (int theIndex = 0; theIndex < theSize; ++theIndex) {
            yield return _elements[theIndex];
        }
    }

    public void iterateWithRemove(ElementPredicate inPredicate, bool inUseSwapRemove = false) {
        if (!XUtils.isValid(inPredicate)) return;

        int theIndex = 0;
        while (theIndex < getSize()) {
            bool theDoRemove = inPredicate.Invoke(_elements[theIndex]);
            if (theDoRemove) {
                removeElementAt(theIndex, inUseSwapRemove);
            } else {
                ++theIndex;
            }
        }
    }

    public void filter<T_FilterType>(T_FilterType inSaveValueFilter, bool inUseSwapRemove = false)
        where T_FilterType : struct, FastFunctors.IFilter<T_ElementType>
    {
        int theIndex = 0;
        while (theIndex < getSize()) {
            if (inSaveValueFilter.testValue(_elements[theIndex])) {
                ++theIndex;
            } else {
                removeElementAt(theIndex, inUseSwapRemove);
            }
        }
    }

    //-Find
    public Optional<int> findIndex(ElementPredicate inPredicate) {
        if (null == inPredicate) return new Optional<int>();
        XUtils.check(null != inPredicate);

        int theIndex = 0;
        foreach (T_ElementType theElement in this) {
            if (inPredicate.Invoke(theElement)) {
                return new Optional<int>(theIndex);
            }
            ++theIndex;
        }

        return new Optional<int>();
    }

    public Optional<T_ElementType> findElement(ElementPredicate inPredicate) {
        Optional<int> theIndex = findIndex(inPredicate);
        return !theIndex.isSet() ? new Optional<T_ElementType>() :
            new Optional<T_ElementType>(this[theIndex.value]);
    }

    public ref T_ElementType findOrCreateElement(
        ElementPredicate inPredicate, ElementAllocDelegate inAllocDelegate)
    {
        XUtils.check(inAllocDelegate);

        Optional<int> theIndex = findIndex(inPredicate);
        if (!theIndex.isSet()) {
            add(inAllocDelegate.Invoke());
            theIndex = new Optional<int>(getLastIndexChecked());
        }
        return ref getRef(theIndex.value);
    }

    public int findOrCreateElement(T_ElementType inElement) {
        Optional<int> theIndex = indexOf(inElement);
        if (theIndex.isSet()) {
            return theIndex.value;
        } else {
            add(inElement);
            return getLastIndexChecked();
        }
    }

    public void collectAll(FastArray<T_ElementType> outElements, ElementPredicate inPredicate) {
        if (null == inPredicate) return;
        foreach (T_ElementType theElement in this) {
            if (!inPredicate(theElement)) continue;
            outElements.add(theElement);
        }
    }

    public void collectAll(T_ElementType[] outElements) {
        int theLastIndex = Mathf.Min(outElements.Length - 1, getLastIndexChecked());
        for (int theIndex = 0; theIndex <= theLastIndex; ++theIndex) {
            outElements[theIndex] = _elements[theIndex];
        }
    }

    //-Implementation
    void reallocate(int inNewSize) {
        T_ElementType[] theOldElements = _elements;

        if (_size > inNewSize) {
            _size = inNewSize;
            _validation_changesTracker.increaseChanges();
        }

        _elements = new T_ElementType[inNewSize];
        int theSize = getSize();
        for (int theIndex = 0; theIndex < theSize; ++theIndex)
            _elements[theIndex] = theOldElements[theIndex];
    }

    IEnumerator IEnumerable.GetEnumerator() { yield return GetEnumerator(); }

    private void validateIndex(int inIndex) {
        XUtils.check(isValidIndex(inIndex),
            "Trying to access element that is out of array: " + inIndex);
    }

    private bool isValidIndex(int inIndex) {
        return isEmpty() ? false : XUtils.isValueInRange(inIndex, 0, getLastIndexChecked());
    }

    //NB: Public for Serialization only
    [SerializeField] public int _size = 0;
    [SerializeField] public T_ElementType[] _elements = null;

    public const int kCapacityIncrement = 4;

    [SerializeField] internal XUtils.ChangesTracker _validation_changesTracker;
}

// --------------------------------------------------

public struct FastArrayWithFirstStackElement<T_ElementType>
{
    public FastArrayWithFirstStackElement(T_ElementType inElement) {
        _firstStackElement = inElement;
        _fastArray = null;
    }

    public FastArrayWithFirstStackElement(FastArray<T_ElementType> inFastArray) {
        XUtils.check(!inFastArray.isEmpty());

        _firstStackElement = default;
        _fastArray = inFastArray;
    }

    public T_ElementType this[int inIndex] {
        get {
            if (XUtils.isValid(_fastArray)) {
                return _fastArray[inIndex];
            } else {
                XUtils.check(0 == inIndex);
                return _firstStackElement;
            } 
        }
    }

    public int getSize() {
        return XUtils.isValid(_fastArray) ? _fastArray.getSize() : 1;
    }

    private T_ElementType _firstStackElement;
    private FastArray<T_ElementType> _fastArray;
}

// --------------------------------------------------

public static class FastArrayUtils
{
    public static void assignFrom<T_ElementType>(
        ref FastArray<T_ElementType> outArrayToAssign, FastArray<T_ElementType> inArrayToAssignFrom,
        bool inFreeMemory = true)
    {
        if (XUtils.isValid(inArrayToAssignFrom)) {
            if (!XUtils.isValid(outArrayToAssign))
                outArrayToAssign = new FastArray<T_ElementType>(inArrayToAssignFrom.getSize(), true);

            outArrayToAssign.assignFrom(inArrayToAssignFrom, inFreeMemory);
        } else {
            if (inFreeMemory && XUtils.isValid(outArrayToAssign))
                outArrayToAssign = null;
        }
    }

    public static void clean<T_ElementType>(FastArray<T_ElementType> inArrayToClean, bool inUseSwapRemove = false)
        where T_ElementType : UnityEngine.Object
    {
        inArrayToClean.filter(new FastFunctors.ValidValueFilter<T_ElementType>());
    }
}

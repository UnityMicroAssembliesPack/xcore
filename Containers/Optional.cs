﻿using System.Collections;
using System.Collections.Generic;

public struct Optional<T_Type>
{
    public Optional(T_Type inValue, bool inIsSet = true) {
        _value = inValue;
        _isSet = inIsSet;
    }

    public bool isSet() { return _isSet; }

    public T_Type value {
        get {
            if (!_isSet) {
                new System.InvalidOperationException();
            }
            return _value;
        }
    }

    public T_Type valueOrDefaultIfNotSet {
        get { return _isSet ? _value : default; }
    }

    public static explicit operator T_Type(Optional<T_Type> inOptional) {
        XUtils.check(inOptional._isSet);
        return inOptional.value;
    }

    public static implicit operator Optional<T_Type>(T_Type inValue) {
        return new Optional<T_Type>(inValue);
    }

    public Optional<T_FieldType> getField<T_FieldType>(System.Func<T_Type, T_FieldType> inQueryFunction) {
        return _isSet ? new Optional<T_FieldType>(inQueryFunction(_value)) : new Optional<T_FieldType>();
    }

    private bool _isSet;
    private T_Type _value;
}


//public class FastStack<T_Type>
//{
//    public FastStack() {
//        _elements = new T_Type[kResizePerRealloc];
//    }
//
//    public T_Type pushNew(System.Func<T_Type> inNewElementsInitializer = null) {
//        expandIfNeeds(inNewElementsInitializer);
//        return _elements[_usedElementsNum++];
//    }
//
//    public T_Type pop() {
//        XUtils.check(_usedElementsNum > 0);
//        return _elements[--_usedElementsNum];
//    }
//
//    public bool isEmpty() {
//        return 0 == _usedElementsNum;
//    }
//
//    public int getSize() {
//        return _usedElementsNum;
//    }
//
//    private void expandIfNeeds(System.Func<T_Type> inNewElementsInitializer) {
//        if (_usedElementsNum == _elements.Length) {
//            int theNewElementNum = _usedElementsNum + kResizePerRealloc;
//
//            T_Type[] theOldElements = _elements;
//            _elements = new T_Type[theNewElementNum];
//
//            int theIndex = 0;
//            for (; theIndex < _usedElementsNum; ++theIndex)
//                _elements[theIndex] = theOldElements[theIndex];
//
//            if (XUtils.isValid(inNewElementsInitializer))
//                for (; theIndex < theNewElementNum; ++theIndex)
//                    _elements[theIndex] = inNewElementsInitializer.Invoke();
//
//            _usedElementsNum = theNewElementNum;
//        }
//    }
//
//    //Fields
//    private T_Type[] _elements = null;
//    private int _usedElementsNum = 0;
//
//    private static int kResizePerRealloc = 4;
//}

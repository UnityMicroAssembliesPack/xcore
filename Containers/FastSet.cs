﻿using UnityEngine;

public class FastSet<T_ElementType> : System.IDisposable
{
    public delegate bool ElementPredicate(T_ElementType inValue);

    public void Dispose() {
        clear(false);
    }

    public void clear(bool inClearMemory = true) {
        _elements.clear(inClearMemory);
    } 

    public bool add(T_ElementType inElement) {
        if (_elements.contains(inElement)) return false;

        _elements.add(inElement);
        return true;
    }

    public bool remove(T_ElementType inElement) {
        if (!_elements.contains(inElement)) return false;

        _elements.removeElement(inElement);
        return true;
    }

    public void collectValues(FastArray<T_ElementType> outArray) {
        outArray.assignFrom(_elements);
    }

    public void iterateWithRemove(ElementPredicate inPredicate){
        if (!XUtils.isValid(inPredicate)) return;

        _elements.iterateWithRemove(
            (T_ElementType inElement)=>{ return inPredicate.Invoke(inElement); }, true
        );
    }

    public bool contains(T_ElementType inElement) {
        return _elements.contains(inElement);
    }

    private FastArray<T_ElementType> _elements = new FastArray<T_ElementType>();
}

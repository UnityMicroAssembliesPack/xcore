﻿

public static class FastFunctors
{
    public interface IFilter<T_ArgType>
    {
        bool testValue(T_ArgType inValue);
    }

    public struct ValidValueFilter<T_Type> : FastFunctors.IFilter<T_Type>
        where T_Type : UnityEngine.Object
    {
        public bool testValue(T_Type inValue) {
            return XUtils.isValid(inValue);
        }
    }
}

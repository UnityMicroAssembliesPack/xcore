﻿public struct EnumBitMask<T_Enum> where T_Enum : System.Enum
{
    //Methods
    //-API
    public EnumBitMask(bool inSetAll = false) {
        _flags = 0;
        setAll(inSetAll);
    }

    public EnumBitMask(params T_Enum[] inInitialSetValues) {
        _flags = 0;
        foreach (T_Enum theInitialSetValue in inInitialSetValues)
            set(theInitialSetValue, true);
    }

    public void set(T_Enum inValue, bool inFlag) {
        int theIndex = getIndexOfValue(inValue);
        XUtils.check(-1 != theIndex);

        _flags = inFlag ? _flags | 1U << theIndex :
            _flags & ~(1U << theIndex);
    }

    public bool get(T_Enum inValue) {
        int theIndex = getIndexOfValue(inValue);
        XUtils.check(-1 != theIndex);
        return (0 != (_flags & 1U << theIndex));
    }

    public void setAll(bool inFlags) {
        _flags = inFlags ? ~0U : 0U;
    }

    public bool isNoSet() {
        return 0 == _flags;
    }
    public bool isAnySet() {
        return 0 != _flags;
    }

    //-Implementation
    //TODO: Use just Enum value, without loop?
    private int getIndexOfValue(T_Enum inValue) {
        int theIndex = 0;
        foreach (object theValue in getEnumValues()) {
            if (inValue.Equals((T_Enum)theValue)) return theIndex;
            ++theIndex;
        }
        return -1;
    }

    private System.Array getEnumValues() {
        return System.Enum.GetValues(typeof(T_Enum));
    }

    //Fields
    //TODO: Use "bit flags set" values here
    //NB: There is may be some probles with serializing.
    // Maybe serialize by enum name, not by value
    uint _flags; 
}

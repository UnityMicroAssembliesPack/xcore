﻿public class Box<T_Type>
{
    public Box(T_Type inValue) {
        value = inValue;
    }

    public override bool Equals(object inObject) {
        if (!(inObject is Box<T_Type>)) return false;
        return _value.Equals(inObject);
    }

    public static implicit operator T_Type(
        Box<T_Type> inValue)
    {
        XUtils.check(inValue);
        return inValue.value;
    }

    public ref T_Type value {
        get { return ref _value; }
    }

    //NB: Just to prevent warning
    public override int GetHashCode() { return base.GetHashCode(); }

    T_Type _value;
}

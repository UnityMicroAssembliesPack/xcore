﻿using System.Collections;
using System.Collections.Generic;

//public class FastPool<T_Type> : IEnumerable<T_Type>
//    where T_Type : System.IEquatable<T_Type> 
//{
//    //Methods
//    //-API
//    public FastPool(System.Func<T_Type> inAllocationFunction,
//        System.Action<T_Type> inDeallocationFunction = null)
//    {
//        _allocationFunction = XUtils.verify(inAllocationFunction);
//        _deallocationFunction = inDeallocationFunction;
//    }
//
//    public FastPool(System.Func<T_Type> inAllocationFunction,
//        System.Action<T_Type> inInitializationFunction,
//        System.Action<T_Type> inDeinitializationFunction,
//        System.Action<T_Type> inDeallocationFunction = null)
//    {
//        _allocationFunction = XUtils.verify(inAllocationFunction);
//        _initializationFunction = XUtils.verify(inInitializationFunction);
//        _deinitializationFunction = XUtils.verify(inDeinitializationFunction);
//        _deallocationFunction = inDeallocationFunction;
//    }
//
//    public T_Type create() {
//        if (_freeIndices.isEmpty()) {
//            expand();
//        }
//
//        int theIndex = _freeIndices.pop();
//        _usedIndices.add(theIndex);
//
//        T_Type theObject = _objects[theIndex];
//        _initializationFunction?.Invoke(theObject);
//        return theObject;
//    }
//
//    public void free(T_Type inObject) {
//        Optional<int> theIndex = _objects.indexOf(inObject); XUtils.check(theIndex.isSet());
//        _deinitializationFunction?.Invoke(inObject);
//
//        _freeIndices.push(theIndex.value);
//        _usedIndices.removeElement(theIndex.value, true);
//    }
//
//    public IEnumerator<T_Type> GetEnumerator() {
//        int theSize = _usedIndices.getSize();
//        for (int theIndex = 0; theIndex < theSize; ++theIndex) {
//            yield return _objects[_usedIndices[theIndex]];
//        }
//    }
//    IEnumerator IEnumerable.GetEnumerator() { yield return GetEnumerator(); }
//
//    //-Implementation
//    private void expand() {
//        XUtils.check(_allocationFunction);
//
//        int theOldSize = _objects.getSize();
//        _objects.addCount(kCapacityIncrement, _allocationFunction);
//
//        for (int theIndex = 0; theIndex < kCapacityIncrement; ++theIndex) {
//            _freeIndices.push(theOldSize + theIndex);
//        }
//    }
//
//    //Fields
//    private System.Func<T_Type> _allocationFunction;
//    private System.Action<T_Type> _initializationFunction;
//    private System.Action<T_Type> _deinitializationFunction;
//    private System.Action<T_Type> _deallocationFunction; //TODO: May be used for pool clearing
//
//    private FastArray<T_Type> _objects = new FastArray<T_Type>();
//    private FastStack<int> _freeIndices = new FastStack<int>();
//    private FastArray<int> _usedIndices = new FastArray<int>();
//
//    private const int kCapacityIncrement = 4;
//}

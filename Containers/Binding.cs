﻿public struct Binding<T_Type> {
    public Binding(System.Func<T_Type> inGet, System.Action<T_Type> inSet) {
        _get = XUtils.verify(inGet);
        _set = XUtils.verify(inSet);
    }

    public T_Type value {
        get { return XUtils.verify(_get).Invoke(); }
        set { XUtils.verify(_set).Invoke(value); }
    }

    public bool isValid() {
        return XUtils.isValid(_get) && XUtils.isValid(_set);
    }

    private System.Func<T_Type> _get;
    private System.Action<T_Type> _set;
}

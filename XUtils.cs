﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

#if UNITY_EDITOR
using static UnityEditor.Events.UnityEventTools;
using UnityEditor.Experimental.SceneManagement;
#endif

public class XUtils : MonoBehaviour
{
    // --- ALGORITHMS --- 
    // - - Basics - -

    public static void swap<T_Type>(ref T_Type inValueRefA, ref T_Type inValueRefB) {
        T_Type theSwapValue = inValueRefA;
        inValueRefA = inValueRefB;
        inValueRefB = theSwapValue;
    }

    public static bool isValueInRange(int inValue, int inMin, int inMax) {
        check(inMin <= inMax);
        return inValue >= inMin && inValue <= inMax;
    }

    public enum Comparation {
        ALessThenB,
        AEqualsB,
        AMoreThenB
    }

    public static Comparation compare<T_Type>(T_Type inA, T_Type inB)
        where T_Type : System.IComparable<T_Type>
    {
        int theComparationResult = inA.CompareTo(inB);
        return 0 == theComparationResult ? Comparation.AEqualsB :
            (theComparationResult > 0 ?
                Comparation.AMoreThenB : Comparation.ALessThenB);
    }

    public static bool areEquals<T_Type>(T_Type inA, T_Type inB) {
        if (typeof(T_Type).IsPrimitive) {
            return EqualityComparer<T_Type>.Default.Equals(inA, inB);
        } else {
            object theAReference = inA;
            object theBReference = inB;
            return theAReference == theBReference;
        }
    }

    public static bool createIfNo<T_Type>(ref T_Type inRef, System.Func<T_Type> inCreateFunction)
        where T_Type : class
    {
        check(inCreateFunction);
        if (null != inRef) return false;

        inRef = inCreateFunction.Invoke();
        return true;
    }

    // - - Array - -

    public static T_Type[] cloneArray<T_Type>(T_Type[] inArray) {
        return (T_Type[])inArray.Clone();
    }

    public static void printArray<T_Type>(T_Type[] inArray) {
        if (null != inArray) {
            int theNum = inArray.Length;
            for (int theIndex = 0; theIndex < theNum; ++theIndex) {
                Debug.Log("[" + theIndex + "] " + inArray[theIndex].ToString());
            }
        } else {
            Debug.Log("<null array>");
        }
    }

    public static bool arrayContains<T_Type>(T_Type[] inArray, T_Type inValue) {
        return -1 != System.Array.IndexOf(inArray, inValue);
    }

    public static void sort<T_Type>(
        T_Type[] inoutArray, int inStartIndex, int inLength,
        System.Func<T_Type, T_Type, Comparation> inLambda)
    {
        check(isValid(inLambda));
        System.Array.Sort(inoutArray, inStartIndex, inLength, LambdaComparer<T_Type>.getForLambda(inLambda));
    }

    public static void sort<T_Type>(FastArray<T_Type> inoutArray, System.Func<T_Type, T_Type, Comparation> inLambda) {
        check(isValid(inLambda));
        System.Array.Sort(inoutArray._elements, 0, inoutArray._size, LambdaComparer<T_Type>.getForLambda(inLambda));
    }

    public static void addSorted<T_Type>(FastArray<T_Type> inoutArray, T_Type inValue,
        System.Func<T_Type, T_Type, Comparation> inLambda)
    {
        //TODO: Optimize assuming that array is currently sorted
        inoutArray.add(inValue);
        sort(inoutArray, inLambda);
    }

    public static void reverseArray<T_Type>(T_Type[] inoutArray) {
        check(inoutArray);

        int theLastIndex = inoutArray.Length - 1;
        if (theLastIndex < 1) return;

        int theElementsToProcess = inoutArray.Length / 2;
        for (int theIndex = 0; theIndex < theElementsToProcess; ++theIndex) {
            swap(ref inoutArray[theIndex], ref inoutArray[theLastIndex - theIndex]);
        }
    }

    public static void setArray<T_Type>(T_Type[] inArray, T_Type inValue) {
        check(inArray);

        int theNum = inArray.Length;
        for (int theIndex = 0; theIndex < theNum; ++theIndex) {
            inArray[theIndex] = inValue;
        }
    }

    public static bool arraysAreEquals<T_Type>(T_Type[] inA, T_Type[] inB) {
        if (null == inA) return (null == inB);

        int theNum = inA.Length;
        if (theNum != inB.Length) return false;

        for (int theIndex = 0; theIndex <theNum; ++theIndex) {
            if (!inA[theIndex].Equals(inB[theIndex])) return false;
        }
        return true;
    }

    public static void cleanCollection<T_Type>(FastArray<T_Type> inArray, bool inUseSwapRemove = false) {
        inArray.iterateWithRemove((T_Type inValue)=>{ return !isValid(inValue); }, inUseSwapRemove);
    }

    public static void cleanCollection<T_Type>(FastSet<T_Type> inSet) {
        inSet.iterateWithRemove((T_Type inValue) => { return !isValid(inValue); });
    }

    public static int min<T_Type>(System.Func<T_Type, T_Type, Comparation> inComparator, params T_Type[] inArray) {
        check(isValid(inArray));
        check(isValid(inComparator));

        int theIndexForMinElement = 0;
        int theLength = inArray.Length;
        for (int theIndex = 1; theIndex < theLength; ++theIndex) {
            if (Comparation.ALessThenB == inComparator.Invoke(inArray[theIndex], inArray[theIndexForMinElement])) {
                theIndexForMinElement = theIndex;
            }
        }
        return theIndexForMinElement;
    }

    // --- VALIDATION --- 

    [System.Serializable]
    public class NullableClass {
        public void setAsNull() {
            _isNotNull = false;
        }

        public void setAsNotNull() {
            _isNotNull = true;
        }

        public bool isNotNull() {
            return _isNotNull;
        }

        [SerializeField, HideInInspector] public bool _isNotNull = false;
    }

    public static bool isValid(object inObject) {
        return (null != inObject);
    }

    public static bool isValid(NullableClass inNullableClass) {
        return (null != inNullableClass && inNullableClass.isNotNull());
    }

    public static bool isValid(Object inObject) {
        return (inObject && null != inObject && !inObject.name.Equals(kDestroyedName));
    }

    public static bool isValid(Component inComponent) {
        return isValid((Object)inComponent) && isValid(inComponent.gameObject);
    }

#if UNITY_EDITOR
    public static bool isPrefabContext(Object inContextObject) {
        return (PrefabStageUtility.GetCurrentPrefabStage() != null);
    }
#endif

    public static void check(
        bool inCondition, string inErrorMessage = "<No message>")
    {
        if (inCondition) return;
        throw new System.Exception(inErrorMessage);
    }

    public static void check(
        Object inUnityObject, string inErrorMessage = "<No message>")
    {
        if (isValid(inUnityObject)) return;
        throw new System.Exception(inErrorMessage);
    }

    public static void check(
        object inObject, string inErrorMessage = "<No message>")
    {
        if (isValid(inObject)) return;
        throw new System.Exception(inErrorMessage);
    }

    public static T_Type verify<T_Type>(
        T_Type inReference, string inErrorMessage = "<No message>")
            where T_Type : class
    {
        if (null != inReference) return inReference;
        throw new System.Exception(inErrorMessage);
    }

    public struct ChangesTracker {
        public void reset() {
            _changesCounter = 0;
        }

        public void increaseChanges() {
            ++_changesCounter;
        }

        public bool Equals(ChangesTracker inOtherTracker) {
            return _changesCounter == inOtherTracker._changesCounter;
        }

        private int _changesCounter;
    }

    // ~ ~ ~ Unity persisten listeners

    public static void cleanListeners(UnityEventBase inEvent) {
#if UNITY_EDITOR
        if (null == inEvent) return;

        int theEventsCount = inEvent.GetPersistentEventCount();
        int theIndex = 0;
        while (theIndex < theEventsCount) {
            if (isValid(inEvent.GetPersistentTarget(theIndex))) {
                ++theIndex;
            } else {
                RemovePersistentListener(inEvent, theIndex);
                --theEventsCount;
            }
        }
#endif
    }

    // --- COMPONENTS ACCESS --- 

    public enum AccessPolicy {
        JustFind,
        ShouldExist,
        CreateIfNo,
        ShouldBeCreated,
        ShouldNoExist,
        RemoveIfExist
    }

    public static T_Type getComponent<T_Type>(GameObject inGameObject,
        AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind,
        System.Action<T_Type> inConstructionLogic = null)
        where T_Type : Component
    {
        T_Type theComponent = verify(inGameObject).GetComponent<T_Type>();
        if (theComponent) {
            switch (inComponentAccessPolicy) {
                case AccessPolicy.ShouldBeCreated:
                case AccessPolicy.ShouldNoExist:
                    check(false);
                    break;

                case AccessPolicy.RemoveIfExist:
                    Destroy(theComponent);
                    theComponent = null;
                    break;
            }
        } else {
            switch(inComponentAccessPolicy) {
                case AccessPolicy.CreateIfNo:
                case AccessPolicy.ShouldBeCreated:
                    theComponent = inGameObject.AddComponent<T_Type>();
                    inConstructionLogic?.Invoke(theComponent);
                    break;

                case AccessPolicy.ShouldExist:
                    check(false);
                    break;
            }
        }

        return theComponent;
    }

    public static T_Type getComponent<T_Type>(Component inGameObjectComponent,
        AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind,
        System.Action<T_Type> inConstructionLogic = null)
        where T_Type : Component
    {
        check(inGameObjectComponent);
        return getComponent(inGameObjectComponent.gameObject, inComponentAccessPolicy, inConstructionLogic);
    }

    public static T_Type getComponentInChildren<T_Type>(GameObject inGameObject,
        AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind)
        where T_Type : Component
    {
        T_Type theComponent = verify(inGameObject).GetComponentInChildren<T_Type>();
        switch(inComponentAccessPolicy) {
            case AccessPolicy.JustFind:       return theComponent;
            case AccessPolicy.ShouldExist:    return verify(theComponent);
            case AccessPolicy.ShouldNoExist:  check(!isValid(theComponent)); return theComponent;
            default:                          check(false); return null;
        }
    }

    public static T_Type getComponentInChildren<T_Type>(Component inGameObjectComponent,
        AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind)
        where T_Type : Component
    {
        check(inGameObjectComponent);
        return getComponentInChildren<T_Type>(inGameObjectComponent.gameObject, inComponentAccessPolicy);
    }

    public static void collectComponentsInChildren<T_Type>(FastArray<T_Type> outComponents,
        GameObject inGameObject, AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind)
        where T_Type : Component
    {
        check(outComponents);
        check(inGameObject);

        List<T_Type> theComponents;
        using (StackObjects.getTop(out theComponents)) {
            inGameObject.GetComponentsInChildren(theComponents);
            outComponents.addAll(theComponents);

            switch (inComponentAccessPolicy) {
                case AccessPolicy.JustFind: break;
                case AccessPolicy.ShouldExist: check(!outComponents.isEmpty()); break;
                case AccessPolicy.ShouldNoExist: check(outComponents.isEmpty()); break;
                default: check(false); break;
            }
        }
    }

    public static void getComponentsInChildren<T_Type>(FastArray<T_Type> outComponents,
        GameObject inGameObject, AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind)
        where T_Type : Component
    {
        verify(outComponents).clear(false);
        collectComponentsInChildren(outComponents, inGameObject, inComponentAccessPolicy);
    }

    public static void getComponentsInChildren<T_Type>(FastArray<T_Type> outComponents,
        Component inGameObjectComponent, AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind)
        where T_Type : Component
    {
        check(inGameObjectComponent);
        getComponentsInChildren(outComponents, inGameObjectComponent.gameObject, inComponentAccessPolicy);
    }

    public static GameObject getParentGameObject(GameObject inGameObject) {
        Transform theParentTransform = verify(inGameObject).transform.parent;
        return theParentTransform ? theParentTransform.gameObject : null;
    }

    public static T_Type getComponentInParent<T_Type>(GameObject inGameObject,
        AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind,
        bool inDirectParentOnly = false)
        where T_Type : Component
    {
        check(inGameObject);
        T_Type theComponent = inDirectParentOnly ?
            getParentGameObject(inGameObject)?.GetComponent<T_Type>() :
            inGameObject.GetComponentInParent<T_Type>();

        switch (inComponentAccessPolicy)
        {
            case AccessPolicy.JustFind: return theComponent;
            case AccessPolicy.ShouldExist: return verify(theComponent);
            case AccessPolicy.ShouldNoExist: check(!isValid(theComponent)); return theComponent;
            default: check(false); return null;
        }
    }

    public static T_Type getComponentInParent<T_Type>(Component inGameObjectComponent,
        AccessPolicy inComponentAccessPolicy = AccessPolicy.JustFind,
        bool inDirectParentOnly = false)
        where T_Type : Component
    {
        check(inGameObjectComponent);
        return getComponentInParent<T_Type>(
            inGameObjectComponent.gameObject, inComponentAccessPolicy, inDirectParentOnly
        );
    }

    // --- OBJECTS LIFECYCLE

    public static T_Type createObject<T_Type>(T_Type inObject)
        where T_Type : Component
    {
        return getComponent<T_Type>(
            Instantiate(inObject.gameObject), AccessPolicy.ShouldExist
        );
    }

    public static T_Type createObject<T_Type>(T_Type inObject, Transform inTransform)
        where T_Type : Component
    {
        T_Type theObject = createObject(inObject);

        theObject.transform.position = inTransform.position;
        theObject.transform.rotation = inTransform.rotation;
        theObject.transform.localScale = inTransform.localScale;

        return theObject;
    }

    public static void destroy<T_Type>(T_Type inObject)
        where T_Type : Object
    {
        if (isValid(inObject)) {
            GameObject theAsGameObject = inObject as GameObject;
            if (theAsGameObject) {
                theAsGameObject.name = kDestroyedName;
                theAsGameObject.SetActive(false);
            }
            if (Application.IsPlaying(inObject)) {
                Object.Destroy(inObject);
            } else {
                Object.DestroyImmediate(inObject);    
            }
        }
    }

    // --- OBJECT RELATIONS ---

    public static bool isObjectHasComponent<T_Type>(GameObject inObject, T_Type inComponent)
        where T_Type : Component
    {
        check(inObject);
        check(inComponent);

        return (-1 != System.Array.IndexOf(inObject.GetComponents<T_Type>(), inComponent));
    }

    public static bool isComponentRelatedToObject<T_Type>(GameObject inObject, T_Type inComponent)
        where T_Type : Component
    {
        check(inObject);
        check(inComponent);

        if (isObjectHasComponent(inObject, inComponent)) return true;
        return isObjectRelatedToObject(inObject, inComponent.gameObject);
    }

    private static bool isObjectRelatedToObject(GameObject inObject, GameObject inTestingObject) {
        check(inObject);
        check(inTestingObject);

        if (inObject == inTestingObject) return true;

        Transform theParentTransform = inTestingObject.transform.parent;
        if (!theParentTransform) return false;
        return isObjectRelatedToObject(inObject, theParentTransform.gameObject);
    }

    // --- MISC ---

    public static Vector2 getMouseWorldPosition() {
        if (!Camera.main) return new Vector2();
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    // --- UTILITY TYPES ---

    private class LambdaComparer<T_Type> : IComparer<T_Type>
    {
        public int Compare(T_Type inA, T_Type inB) {
            XUtils.check(null != lambda);
            switch(lambda(inA, inB)) {
                case Comparation.ALessThenB: return -1;
                case Comparation.AEqualsB:   return 0;
                case Comparation.AMoreThenB: return 1;
            }

            XUtils.check(false);
            return 0;
        }

        public static LambdaComparer<T_Type> getForLambda(
            System.Func<T_Type, T_Type, Comparation> inLambda)
        {
            __lambdaComparer.lambda = inLambda;
            return __lambdaComparer;
        }

        public System.Func<T_Type, T_Type, Comparation> lambda;
        private static LambdaComparer<T_Type> __lambdaComparer =
            new LambdaComparer<T_Type>();
    }

    private static string kDestroyedName = "$";

    // --- MOUSE PICKING ---

    static public Vector2 getMouseViewportPosition(Camera inCamera) {
        return verify(inCamera).ScreenToViewportPoint(Input.mousePosition);
    }

    static public Ray getRayFromCameraToMouse(Camera inCamera) {
        Vector2 theMouseViewportPosition = getMouseViewportPosition(verify(inCamera));
        return inCamera.ViewportPointToRay(theMouseViewportPosition);
    }

    static public Vector3 pickPointInScene(Camera inCamera, int inLayerMask = ~0) {
        Ray theRay = getRayFromCameraToMouse(inCamera);
        RaycastHit theHit; Physics.Raycast(theRay, out theHit, 10000.0f, inLayerMask);
        return theHit.point;
    }

    // --- VISIBILITY CONTROL ---

    public static void setVisibilityVisual(GameObject inObject, bool inIsVisible) {
        verify(inObject).SetActive(inIsVisible);
    }
    public static void setVisibilityVisual(Component inComponent, bool inIsVisible) {
        setVisibilityVisual(verify(inComponent).gameObject, inIsVisible);
    }

    public static void setVisibilityInHierarchy(GameObject inObject, bool inIsVisible) {
        if (inIsVisible) {
            inObject.hideFlags = inObject.hideFlags & ~HideFlags.HideInHierarchy;
        } else {
            inObject.hideFlags = inObject.hideFlags | HideFlags.HideInHierarchy;
        }
    }
    public static void setVisibilityInHierarchy(Component inComponent, bool inIsVisible) {
        setVisibilityInHierarchy(verify(inComponent).gameObject, inIsVisible);
    }

    // ---------------------------------------------------

    public static T_ReturnType callFuncOrReturn<T_ReturnType, T_Arg0>(
        System.Func<T_Arg0, T_ReturnType> inFunc, T_Arg0 inArg0, T_ReturnType inReturn)
    {
        return isValid(inFunc) ? inFunc(inArg0) : inReturn;
    }
}